﻿namespace netcore31webapisample.Responses
{
    public class Prefecture
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
